# Tooltip-Plugin for TinyMCE 4

This Plugin is written for TinyMCE Version 4.

Copy the Folder `/plugin/tooltip` => `YOURTINYPATH/plugin/tooltip`

Then activate this Plugin in the TinyMCE-init-File

1. in paramter `plugins:` add ' tooltip' in the string
2. you can optional add this in the `toolbar:` and ' tooltip' on the right position from string
3. clean Cache and reload the Browser

If the span-tag of the tooltip should be deleted again, the title field must be saved empty in the tooltip settings.

![tinymce4_tooltip_settings](src/images/tooltip_settings.png "Settings from the Tooltip-Plugin")
![tinymce4_tooltip_fe](src/images/tooltip_fe.png "Tooltip on Website")
