(function () {
var tooltip = (function () {
    'use strict';

    var global = tinymce.util.Tools.resolve('tinymce.PluginManager');

    var getTitle = function (editor) {
      var selectedNode = editor.selection.getNode();
      var isTitle = selectedNode.tagName === 'SPAN' && editor.dom.getAttrib(selectedNode, 'title') !== '';
      return isTitle ? selectedNode.title : '';
    };
    var getClasses = function (editor) {
      var selectedNode = editor.selection.getNode();
      var isClasses = selectedNode.tagName === 'SPAN' && editor.dom.getAttrib(selectedNode, 'class') !== '';
      // console.log(editor.dom.getAttrib(selectedNode, 'class'));
      return isClasses ? editor.dom.getAttrib(selectedNode, 'class') : 'tinymce_tooltip';
    };

    var insert = function (editor, title, classes) {

      var selectedNode = editor.selection.getNode();
      var isTooltip = selectedNode.tagName === 'SPAN' && editor.dom.getAttrib(selectedNode, 'title') !== '';
      var spanAttrs = {};

      if (isTooltip) {
        //wenn der Titel leer ist das span-ELement wieder entfernen
        if(title === '') {
            spanAttrs = {
                class: null,
                title: null
            };
        } else {
            spanAttrs = {
                title: title ? title: null,
                class: classes ? classes : null
            };
        }
        editor.dom.setAttribs(selectedNode, spanAttrs);
        editor.undoManager.add();

      } else {
        editor.focus();
        editor.selection.setContent('<span title="'+title+'" class="'+classes+'">' + editor.selection.getContent() + '</span>');
      }
    };
    var getTooltip = function (editor, elm) {
        return editor.dom.getParent(elm, 'span[title]');
    };
    var getSelectedTooltip = function (editor) {
        return getTooltip(editor, editor.selection.getStart());
    };

    var Tooltip = {
        getTitle: getTitle,
        getClasses: getClasses,
        insert: insert,
        getSelectedTooltip: getSelectedTooltip
    };

    var insertTooltip = function (editor, newTitle, newClass) {
        Tooltip.insert(editor, newTitle,newClass);
        return false;
    };
    var open = function (editor) {
      var currentTitle = Tooltip.getTitle(editor);
      var currentClasses = Tooltip.getClasses(editor);

      editor.windowManager.open({
        title: 'Tooltip',
        body: [
            {
                type: 'textbox',
                name: 'title',
                size: 40,
                label: 'Title',
                value: currentTitle
            },
            {
                type: 'textbox',
                name: 'class',
                size: 40,
                label: 'Class',
                value: currentClasses
            },
        ],
        onsubmit: function (e) {
          var newTitle = e.data.title;
          var newClass = e.data.class;
          if (insertTooltip(editor, newTitle,newClass)) {
            e.preventDefault();
          }
        }
      });
    };
    var Dialog = { open: open };

    var register = function (editor) {
      editor.addCommand('mceTooltip', function () {
        Dialog.open(editor);
      });
    };
    var Commands = { register: register };

    var isTooltipNode = function (node) {
      return node.attr('title') && !node.firstChild;
    };
    var setContentEditable = function (state) {
      return function (nodes) {
        for (var i = 0; i < nodes.length; i++) {
          if (isTooltipNode(nodes[i])) {
            nodes[i].attr('contenteditable', state);
          }
        }
      };
    };
    var setup = function (editor) {
      editor.on('PreInit', function () {
        editor.parser.addNodeFilter('span', setContentEditable('false'));
        editor.serializer.addNodeFilter('span', setContentEditable(null));
      });
    };
    var FilterContent = { setup: setup };

    var register$1 = function (editor) {
      editor.addButton('tooltip', {
        icon: 'info',
        tooltip: 'Tooltip',
        cmd: 'mceTooltip',
        stateSelector: 'span'
      });
      editor.addMenuItem('tooltip', {
        icon: 'info',
        text: 'tooltip',
        context: 'insert',
        cmd: 'mceTooltip'
      });
    };
    var Buttons = { register: register$1 };

    global.add('tooltip', function (editor) {
      FilterContent.setup(editor);
      Commands.register(editor);
      Buttons.register(editor);
    });
    function Plugin () {
    }

    return Plugin;

}());
})();
